#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "deb-ver.h"

int main(int argc, char const *argv[]) {
  char i[64], k[64], op = ' ';
  int result = 0;
  scanf("%s", i);
  scanf("%s", k);
  result = verrevcmp(i, k);
  if (result < 0) {
    op = '<';
  } else if(result == 0) {
    op = '=';
  } else {
    op = '>';
  }
  printf("%s %c %s\n", i, op, k);
  return 0;
}
