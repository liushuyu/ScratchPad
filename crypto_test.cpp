#include "crypto.c"
#include <iostream>

int main(int argc, char const *argv[]) {
  if (argc < 2) {
    return 1;
  }
  char* result = NULL;
  errno = 0;
  int ret = hash_file(argv[1], "sha384", &result);
  if (ret == 0) {
    std::cout << result << std::endl;
  } else {
    std::cout << "Failed to get digest: " <<  strerror(ret) << std::endl;
  }
  free(result);
  return 0;
}
