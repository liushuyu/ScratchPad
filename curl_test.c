#include <curl/curl.h>
#include <stdio.h>
#include <unistd.h>
#include <curl/curl.h>
/* For older cURL versions you will also need
#include <curl/easy.h>
#include <curl/types.h>
*/

size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
  size_t written = fwrite(ptr, size, nmemb, stream);
  return written;
}

int curl_fetch(const char* url, const char* dest) {
  CURL *curl = NULL;
  FILE *fp = NULL;
  CURLcode res = CURLE_OK;
  long response_code = 0;
  curl_off_t cl = 0;
  int status = 0;

  curl = curl_easy_init();
  if (curl) {
    fp = fopen(dest, "wb");
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
    res = curl_easy_perform(curl);
    if (res == CURLE_OK) {
      res = curl_easy_getinfo(curl, CURLINFO_SIZE_DOWNLOAD_T, &cl);
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
      if (response_code >= 400 && response_code < 500) {
          printf("Server returned error: %ld\n", response_code);
          status = 1;
      }
      if (response_code >= 500) {
          printf("Server is having problem: %ld\n", response_code);
          status = 2;
      }
    }
    /* always cleanup */
    curl_easy_cleanup(curl);
    fclose(fp);
  }
  return status;
}

int main(void) {
  return curl_fetch("https://github.com/flightaware/dump1090/archive/v3.5.0.tar.xz", "/tmp/test.bin");
}
