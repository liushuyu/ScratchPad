#include "deps_v2.cpp"  // hack

int main(int argc, char const *argv[]) {
  packagelist_t tasks(3);
  tasks[0].name = "a";
  tasks[0].missingdeps = {"b"};
  tasks[1].name = "b";
  tasks[1].missingdeps = {};
  tasks[2].name = "c";
  tasks[2].missingdeps = {"a", "d"};
  // tasks[0] = {.name = "a", .deps = {}};
  // tasks[1] = {.name = "b", .deps = {"c"}};
  // tasks[2] = {.name = "c", .deps = {}};
  packagelist_t solved(0);
  calc_deps(tasks, solved);
  for (simplePackage task : solved) {
      cout << task.name << " ";
  }
  cout << endl;
  return 0;
}
