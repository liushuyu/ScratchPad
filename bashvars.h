#ifndef BASHVARS_H
#define BASHVARS_H
#include <sys/wait.h>
#include <unistd.h>
#include <iostream>
#include <vector>

#include "inih/INIReader.h"
typedef std::vector<std::string> stringlist;

class bashVars
{
public:
	bashVars(const std::string scriptname);
	~bashVars();
	stringlist getAllVarNames();
	std::string getVarByName(const std::string varname);
	bool hasVar(const std::string varname);
	bool isErrored();
private:
	INIReader* parser;
	std::string scriptOutput;
	int readScript(const std::string path, std::string& output);
	bool errored;
};

#endif // BASHVARS_H
