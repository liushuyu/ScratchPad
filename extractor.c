#include <magic.h>
#include <archive.h>
#include <archive_entry.h>
#include <string.h>
#include <stdbool.h>

// #include "acbs_utils.h"

bool file_type(const char* filename, bool mime_only, char** result) {
	int mc_flags = mime_only ? MAGIC_MIME_TYPE : MAGIC_NONE;
	magic_t mc = magic_open(mc_flags);
  if (mc == NULL) return false;
  magic_load(mc, NULL);
	*result = strdup(magic_file(mc, filename));
  if (!result) {
    *result = strdup(magic_error(mc));
    return false;
  }
	magic_close(mc);
	if (!result) return false;
	return true;
}

ssize_t test_archive(const char* path, char** msg)
{
	struct archive *ar;
	struct archive_entry *entry;
	ssize_t entries = 0;
	ar = archive_read_new();
	archive_read_support_filter_all(ar);
	archive_read_support_format_all(ar);
	if (archive_read_open_filename(ar, path, 4096) != ARCHIVE_OK)
	{
		*msg = strdup(archive_error_string(ar));
		return 0;
	}
	while (archive_read_next_header(ar, &entry) == ARCHIVE_OK)
	{
		// Do nothing
	}
	entries = archive_file_count(ar);
	if (archive_read_free(ar) != ARCHIVE_OK)
	{
		*msg = strdup(archive_error_string(ar));
		return 0;
	}
	return entries;
}

int main(int argc, char const *argv[]) {
  char* ret = NULL;
  file_type(argv[0], true, &ret);
  printf("%s\n", ret);
  return 0;
}
