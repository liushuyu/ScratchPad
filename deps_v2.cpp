#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include "acbs_adt.h"

using namespace std;

packagelist_t fetch_deps(string name) {
  packagelist_t result(2);
  result[0].name = "d";
  result[0].missingdeps = {};
  result[1].name = "f";
  result[1].missingdeps = {"c"};
  return result;
}

void removeAll(string target, packagelist_t& pkgs) {
  for (simplePackage &other : pkgs) {
    for (auto dep = other.missingdeps.begin(); dep != other.missingdeps.end();) {
      if (*dep == target) {
        dep = other.missingdeps.erase(dep);
        continue;
      }
      ++dep;
    }
  }
}


// following algorithm describes a "graph reduce" method
int calc_deps(packagelist_t pkgs, packagelist_t& solved) {
  size_t remained_task_num = pkgs.size();
  bool tried_fetch = false;
  while (remained_task_num > 0) {
    packagelist_t indeps;
    for (auto pkg = pkgs.begin(); pkg != pkgs.end();) {  // Iterate each task to see whether it has any deps
      if (pkg->missingdeps.empty()) {  // if no, just move it to no-dependency array
        tried_fetch = false;
        indeps.push_back(*pkg);
        remained_task_num = remained_task_num ? remained_task_num - 1 : 0;
        // remove this package from missingdeps in each package
        removeAll(pkg->name, pkgs);
        pkg = pkgs.erase(pkg);
        continue;
      } // ! if tasks deps empty
      ++pkg;
    } // ! for task

    if (indeps.empty()) {
      // some extra here: this part is not part of the GR algorithm
      if (tried_fetch) {
        cout << "Circular dependency detected." << endl;
        return -1;
      } else {
        tried_fetch = true;
        // iterate each package to get missing deps
        for (simplePackage pkg : pkgs) {
          // iterate each dep in package
          for (string dep : pkg.missingdeps) {
            bool found = false;
            // try to see if this dep already included in the queue
            for (simplePackage opkg : pkgs) {
              if (dep == opkg.name) {
                found = true;
                break;
              }
            } // ! other package
            // try to see if this dep already resolved
            for (simplePackage opkg : solved) {
              if (dep == opkg.name) {
                found = true;
                break;
              }
            }
            if (!found) {
              packagelist_t ret = fetch_deps(dep);
              remained_task_num += ret.size();
              // remove already resolved packages from the deps list
              // of newly added packages
              for (simplePackage& indep : solved) {
                removeAll(indep.name, ret);
              }
              copy(ret.begin(), ret.end(), back_inserter(pkgs));
              // fetch deps
            }
          } // ! dep
        } // ! pkg examine
      } // ! else clause
    } // ! if indeps is empty

    solved.reserve(indeps.size());
    copy(indeps.begin(), indeps.end(), back_inserter(solved));
  }  // ! while
  return 0;
}
