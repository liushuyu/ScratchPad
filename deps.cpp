#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>

using namespace std;

typedef std::vector<string> stringlist;

struct task{
  string name;
  stringlist deps;
};

typedef struct task task_t;
typedef std::vector<task_t> task_list_t;

int calc_deps(task_list_t tasks, task_list_t& solved) {
  size_t remained_task_num = tasks.size();
  while (remained_task_num > 0) {
    task_list_t indeps;
    for (auto task = tasks.begin(); task != tasks.end();) {  // Iterate each task to see whether it has any deps
      if (task->deps.empty()) {  // if no, just move it to no-dependency array
        indeps.push_back(*task);
        remained_task_num = remained_task_num ? remained_task_num - 1 : 0;
        // remove this task from all the task deps
        for (task_t &other : tasks) {
          for (auto dep = other.deps.begin(); dep != other.deps.end();) {
            if (*dep == task->name) {
              dep = other.deps.erase(dep);
              continue;
            }
            ++dep;
          }
        }
        task = tasks.erase(task);
        continue;
      } // ! if tasks deps empty
      ++task;
    } // ! for task

    if (indeps.empty()) {
      cout << "Circular dependency detected." << endl;
      return -1;
    }

    solved.reserve(indeps.size());
    copy(indeps.begin(), indeps.end(), back_inserter(solved));
  }  // ! while
  return 0;
}

