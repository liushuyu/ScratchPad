#include <openssl/evp.h>
#include <openssl/opensslv.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>


#ifndef OPENSSL_MAKE_VERSION
#define OPENSSL_MAKE_VERSION(maj,min,fix,patch) ((0x10000000L)+((maj&0xff)<<20)+((min&0xff)<<12)+((fix&0xff)<<4)+patch)
#endif

#ifndef OPENSSL_VERSION_AT_LEAST
#define OPENSSL_VERSION_AT_LEAST(maj,min) (OPENSSL_VERSION_NUMBER >= OPENSSL_MAKE_VERSION(maj,min, 0, 0))
#endif

int hash_file(const char* filename, const char* hashType, char** result)
{
	EVP_MD_CTX *mdctx;
	const EVP_MD *md;
	unsigned char md_value[EVP_MAX_MD_SIZE];
	char md_repr[EVP_MAX_MD_SIZE << 1];
	ssize_t fsize = 0;
	char* mappedFile;
	unsigned int digest_len;
	OpenSSL_add_all_digests();  // Initialize all digest libraries
	md = EVP_get_digestbyname(hashType);
	if (!md) {
		return -1;
	}
	struct stat sb;
	int fd = open(filename, O_RDONLY);
	if (fd == -1) {
		return errno;
	}
	if (fstat(fd, &sb) == -1) {
		return errno;
	}
	mappedFile = (char *)mmap(0, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if (mappedFile == MAP_FAILED) {
		return errno;
	}
	fsize = sb.st_size;
	mdctx =
#if OPENSSL_VERSION_AT_LEAST(1, 1)
  EVP_MD_CTX_new();
#else
  EVP_MD_CTX_create();
#endif
	EVP_DigestInit_ex(mdctx, md, NULL);
	EVP_DigestUpdate(mdctx, mappedFile, fsize);
	EVP_DigestFinal_ex(mdctx, md_value, &digest_len);
#if OPENSSL_VERSION_AT_LEAST(1, 1)
  EVP_MD_CTX_free(mdctx);
#else
	EVP_MD_CTX_destroy(mdctx);
#endif
	EVP_cleanup();
	munmap(mappedFile, fsize);
	for (ssize_t i = 0; i < digest_len; i++) {
		snprintf(md_repr + 2 * i, 4, "%02x", md_value[i]);
	}
  *result = (char*)calloc(2, EVP_MAX_MD_SIZE);
  memcpy(*result, md_repr, EVP_MAX_MD_SIZE << 1);
	return 0;
}
