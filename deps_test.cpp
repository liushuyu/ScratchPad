#include "deps.cpp"  // hack

int main(int argc, char const *argv[]) {
  task_list_t tasks(3);
  tasks[0] = {.name = "a", .deps = {}};
  tasks[1] = {.name = "b", .deps = {"c"}};
  tasks[2] = {.name = "c", .deps = {}};
  task_list_t solved(0);
  calc_deps(tasks, solved);
  for (task_t task : solved) {
      cout << task.name << " ";
  }
  cout << endl;
  return 0;
}

