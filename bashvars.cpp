#include "bashvars.h"

bashVars::bashVars(const std::string scriptname)
{
	errored = false;
	if (this->readScript(scriptname, scriptOutput)) {
		errored = true;
		return;
	}
	parser = new INIReader(scriptOutput, true);
	if (parser->ParseError()) {
		errored = true;
		std::cout << scriptOutput << "." << std::endl;
		return;
	}
}

bashVars::~bashVars()
{
	delete parser;
}

stringlist bashVars::getAllVarNames()
{
	std::set<std::string> fields = parser->GetFields("");
	stringlist v(fields.begin(), fields.end());
	return v;

}

std::string bashVars::getVarByName(const std::string varname)
{
	return parser->Get("", varname, "");
}

bool bashVars::hasVar(const std::string varname)
{
	return parser->Get("", varname, "") == "" ? false : true;
}

bool bashVars::isErrored()
{
	return errored;
}

int bashVars::readScript(const std::string path, std::string &output)
{
	pid_t pid = 0;
	// Pipe: 0: read end, 1: write end
	int pipefd[2];
	char buf[256];
	pipe(pipefd);
	ssize_t buflen = 0;
	int status = -1;
	if ((pid = fork())) {
		close(pipefd[1]);
		while((buflen = read(pipefd[0], &buf, sizeof(buf))) > 0) {
			output.append(buf, buflen);
		}
		waitpid(pid, &status, 0);
		close(pipefd[0]);
	} else {
		dup2(pipefd[1], STDOUT_FILENO);
		close(pipefd[0]);
		execlp("bash", "bash", "--noprofile", "--norc", "-c", ("comm -3 <(set | sort) <((source '" + path + "'; set) | sort) | sed 's/\t//g'").c_str(), NULL);
		perror("execl()");
		return 1;
	}
	return WEXITSTATUS(status);
}
